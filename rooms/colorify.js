const colyseus = require('colyseus');
const ColorifyGame = require('../static/game/game.js');

class Colorify extends colyseus.Room {


  onInit (options) {
    this.game = new ColorifyGame(winnerId => {
      console.log(`The winner is ${winnerId}`);
    });
    this.game.createBoard();
    // console.log(this.game.board);
    // console.log("RoomId: ",this.roomId);

    this.availableGameIds = [0,1,2,3];


    this.maxClients = 4
    console.log("colorify room initted with", options);
    //create map


    this.setState({
      map: this.game.getBoard(),
      players: {},
      turn: 0,
      gameStarted: false
    });
  }

  requestJoin(options, isNew) {
    // console.log("requesting join");
    if (isNew) {
      // console.log("room is new");
      return true;
    } else {
      // console.log("room is not new", options);
      if (typeof options === "undefined") {
        // console.log("options not defined");
        return false;

      }
      if( options.id == this.roomId){
        return true;

      }else {
        // console.log("id was not correct");
        return false;
      }
    }
  }

  onJoin (client) {
    console.log("client joined");
    const cPlayer = this.state.players[client.id];

    if (typeof cPlayer !== "undefined") {
      return;
    } else  if(this.state.gameStarted){
      if (cPlayer.disconnected) {
        cPlayer.disconnected = false;
      }
    } else {
      let gameId = this.getAvailableGameId();
      this.send(client, {gameId: gameId});
      this.state.players[client.id] = {
        nickname: `Player ${gameId}`,
        defaultNickname: true,
        gameId: gameId,
        disconnected: false
      }



      this.game.addPlayer(this.game.colors[gameId], gameId);
      const nicknameObjects = [];
      for (var player in this.state.players) {
        nicknameObjects.push({
          gameId: player.gameId,
          nickname: player.nickname
        });
      }

      this.send(client, {
        action: "nicknames",
        nicknames: nicknameObjects
      })


      if (this.playersConnected == 4) {
        console.log("starting game!")
        //start the game
        this.state.gameStarted = true;
        this.broadcast({
          message: "start game",
          map: this.state.map,
          players: this.state.players
        });

      }
    }






  }

  onLeave (client, consented) {
    console.log("player disconnected");
    if (!this.state.gameStarted) {
      const cPlayer = this.state.players[client.id];
      if (typeof cPlayer == "object") {
        console.log("removing player from the game");
        const gameId = cPlayer.gameId;
        this.availableGameIds.push(gameId);
        this.game.removePlayer(gameId);
        // console.log(this.state.players);
        delete this.state.players[client.id];
        // console.log(this.state.players);

      }
    } else {
      console.log("setting the player to disconnected");
      this.state.players[client.id].disconnected = true;
      this.playIfDisconnected();
    }

  }

  onMessage (client, message) {
    // console.log("gameStarted: " + this.state.gameStarted);
    console.log("message: ", message);

    const cPlayer = this.state.players[client.id];
    const nickname = message.nickname;
    let gameId = cPlayer.gameId

    if (typeof nickname === "string" && cPlayer.defaultNickname && nickname !== "") {
      console.log("nickname changed");
      cPlayer.nickname = nickname;
      cPlayer.defaultNickname = false;
      this.broadcast({
        action: "changed nickname",
        gameId: gameId,
        nickname: nickname
      });
    }

    if (!this.state.gameStarted) {
      return;
    }
    //check if its the clients turn

    console.log("gameid: " + gameId);
    console.log("turn: " + this.state.turn);
    if (gameId == this.state.turn % 4) {
      //validate message
      if (typeof message === "object") {
        let chosenColor = parseInt(message.color);
        console.log("chosen Color: " + chosenColor);
        if ( this.game.isColorAvailable(chosenColor)) {
          this.game.players[gameId].chooseColor(chosenColor);
          this.state.turn++;
          this.state.map = this.game.getBoard();

          this.broadcast({
            action: "choose color",
            colorId: chosenColor,
            gameId: gameId
          });
          this.playIfDisconnected();



          console.log("Color changed");
        }
      }



    } else {
      console.log("its not your turn");
    }
  }

  getAvailableGameId () {
    const availableGameId = this.availableGameIds[0];
    this.availableGameIds.splice(0,1);
    return availableGameId;
  }

  get playersConnected() {
    return 4- this.availableGameIds.length;
  }

  getClientIdFromGameId(gameId){
    for (var player in this.state.players) {
      if (this.state.players.hasOwnProperty(player)) {
        if (this.state.players[player].gameId === gameId) {
          return player;

        }
      }
    }
  }

  playIfDisconnected () {
    //get gameId
    //get the state.player
    //check if they are disconnected
    //then pick a random available color for that player

    const gameId = this.state.turn % 4;
    let cPlayer = this.state.players[this.getClientIdFromGameId(gameId)];

    // console.log("It is currently player's turn", cPlayer);
    // console.log("All players state: ", this.state.players);

    if (cPlayer.disconnected) {
      // console.log("player is disconnected, playing their turn");
      const colors = this.game.getAvailableColors();
      const colorId = colors[Math.floor(Math.random() * colors.length)].id;
      // console.log("chose color: ", colorId);
      this.game.players[gameId].chooseColor(colorId);
      this.state.turn++;
      this.broadcast({
        action: "choose color",
        colorId: colorId,
        gameId: gameId
      });
      this.playIfDisconnected();
    } else {
      // console.log("player is not disconnected");
    }
  }




}


module.exports = Colorify;
