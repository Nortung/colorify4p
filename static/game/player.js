class Player
{
  constructor(color, id)
  {
    this.color = color;
    this.id = id;
  }

  chooseColor(color)
  {
    for (var i = 0; i < game.board.length; i++)
    {
      for (var j = 0; j < game.board[i].length; j++)
      {
        game.board[i][j].checkFieldsAround(color, this.id);
      }
    }
  }
}
