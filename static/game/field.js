class Field
{
  constructor(x, y, color)
  {
    this.x = x;
    this.y = y;
    this.c = color;
    this.id = null;
  }



  draw()
  {
    fill(this.c.r, this.c.g, this.c.b);
    rect(this.x * game.tileSize, this.y * game.tileSize, game.tileSize + this.x * game.tileSize, game.tileSize + this.y * game.tileSize);
  }

  checkFieldsAround( colorChosen, playerId )
  {
    if(this.id == null)
    {
      return;
    }

    if(this.id == playerId)
    {
      if(this.y != 0)
      {
        //Checker over feltet der checkes på tidspunktet
        if(game.board[this.x][this.y - 1].id == playerId && game.board[this.x][this.y - 1].color.id == colorChosen)
        {
          game.board[this.x][this.y - 1].checkFieldsAround(colorChosen, playerId);
        }
      }

      if(this.y != (game.sizeY - 1))
      {
        // Checker under feltet der checkes på tidspunktet
        if(game.board[this.x][this.y + 1].id == playerId && game.board[this.x][this.y + 1].color.id == colorChosen)
        {
          game.board[this.x][this.y + 1].checkFieldsAround(colorChosen, playerId);
        }
      }


      if(this.x != (game.sizeX - 1) )
      {
        // Checker om der er nogen til højre
        if(game.board[this.x + 1][this.y].id == playerId && game.board[this.x + 1][this.y].color.id == colorChosen)
        {
          game.board[this.x + 1][this.y].checkFieldsAround(colorChosen, playerId);
        }
      }

      if(this.x != 0)
      {
        // Checker om der er noget til venstre
        if(game.board[this.x - 1][this.y].id == playerId && game.board[this.x - 1][this.y].color.id == colorChosen)
        {
          game.board[this.x - 1][this.y].checkFieldsAround(colorChosen, playerId);
        }
      }
    }
  }
}
