class Game
{


	constructor(callback)
	{
		this.sizeX = 16; // Amount of points in the x axis
		this.sizeY = 16; // Amount of points in the y axis
		this.tileSize = 25; // Size of the square
		this.players = [];

		this.colors = [
			new RGB( 255, 0	 , 255, 1 ),
			new RGB( 0	, 0	 , 255, 2 ),
			new RGB( 0	, 255, 255, 3 ),
			new RGB( 0	, 255, 0	, 4 ),
			new RGB( 255, 255, 0	, 5 ),
			new RGB( 255, 0	 , 0	, 6 ),
			new RGB( 255, 128, 0	, 7 ),
			new RGB( 127, 0	 , 255, 8 )
		]; // Colors usable in the game
		this.gameStarted = false; // Variable if the game is started
		this.board = []; // Array of board information
    if (typeof callback === "function") {
      this.callback = callback;
    } else {
      this.callback = () => {}
    }

	}


	/*****************************************************************************

	Will create a board for the game to run on!

	******************************************************************************/
	createBoard()
	{
		var board = [];

		for ( var x = 0; x < this.sizeX; x++ )
		{
			var yrow = [];
			for ( var y = 0; y < this.sizeY; y++ )
			{
				var color = this.colors[ Math.floor( Math.random() * this.colors.length ) ];
				yrow.push( new Field( x, y, color, this ) );
			}

			board.push( yrow );
		}

		this.board = board;
		return board;
	}

	getBoard()
	{
		let newBoard = [];

		for ( var i = 0; i < this.board.length; i++ )
		{
			newBoard[ i ] = [];
			for ( var j = 0; j < this.board[ i ].length; j++ )
			{
				newBoard[ i ][ j ] = {
					x: this.board[ i ][ j ].x,
					y: this.board[ i ][ j ].y,
					color: this.board[ i ][ j ].c,
					id: this.board[ i ][ j ].id
				};
			}
		}

		return newBoard;
	}



	setBoard( newBoard )
	{
		//console.table(newBoard);

		this.board = [];
		for ( var i = 0; i < newBoard.length; i++ )
		{
			this.board[ i ] = [];
			for ( var j = 0; j < newBoard[ i ].length; j++ )
			{
				this.board[ i ][ j ] = new Field( newBoard[ i ][ j ].x, newBoard[ i ][ j ].y, newBoard[ i ][ j ].color, this );
				this.board[ i ][ j ].setId( newBoard[ i ][ j ].id );
			}
		}
		//console.log(this.board[0][0]);
		//console.log("Map has been set");
	}

	rotateBoard( board )
	{
		var newBoard = board;

		for ( var i = 0; i < this.sizeX; i++ )
		{
			for ( var j = 0; j < this.sizeY; j++ )
			{
				newBoard[ i ][ j ] = board[ this.sizeX - j - 1, i ];
			}
		}

		return newBoard;
	}

	getAvailableColors()
	{

		let allColors = this.colors;
		let availableColors = [];

		for ( let i = 0; i < allColors.length; i++ )
		{
			let found = false;
			this.players.forEach( player =>
			{
				// console.log(player.color, allColors[i].id);
				if ( player.color.id == allColors[ i ].id )
				{
					found = true;
				}
			} );
			if ( !found )
			{
				availableColors.push( allColors[ i ] );
			}
		}
		return availableColors;
	}

	isColorAvailable( colorId )
	{
		var colors = this.getAvailableColors();
		for ( let i = 0; i < colors.length; i++ )
		{
			if ( colors[ i ].id == colorId )
			{
				return true;
			}
		}
		return false;
	}

	/*****************************************************************************

	Will add a player to the game

	******************************************************************************/
	addPlayer( color, id )
	{
		if ( typeof this.board == "undefined" )
		{
			//console.log("kill me?!");
		}

		// console.log(id);

		this.players[ id ] = ( new Player( color, id, this ) );
		var places = [
			[ 0, 0 ],
			[ this.sizeX - 1, 0 ],
			[ 0, this.sizeY - 1 ],
			[ this.sizeX - 1, this.sizeY - 1 ]
		];

		//console.log("id player add: " + id);

		let cPlace = places[ id ];
		//console.log(cPlace);
		//console.log(this.board[cPlace[0]]);
		this.board[ cPlace[ 0 ] ][ cPlace[ 1 ] ].setId( id );
		this.board[ cPlace[ 0 ] ][ cPlace[ 1 ] ].setColor( color );

		// if(id == 0)
		// {
		//   this.board[places[0][0]][places[0][1]].setId(id);
		// }else if(id == 1){
		//   this.board[places[1][0]][places[1][1]].setId(id);
		// }else if(id == 2){
		//   this.board[places[2][0]][places[2][1]].setId(id);
		// }else if(id == 3){
		//   this.board[places[3][0]][places[3][1]].setId(id);
		// }

		// this.players.push(new Player(color, id));
	}

	removePlayer( id )
	{
		delete this.players[ id ];
	}


	getPlayersFields()
	{


		const playerCountArray = [ 0, 0, 0, 0 ];

		for ( var i = 0; i < this.board.length; i++ )
		{
			for ( var j = 0; j < this.board[ i ].length; j++ )
			{
				const fieldId = this.board[ i ][ j ].id;
				if ( fieldId == null )
				{
					continue;
				}
				playerCountArray[ fieldId ]++;
			}

		}
		return playerCountArray;

	}


	checkWin()
	{
		const playerFields = this.getPlayersFields();
		const playerFields2 = playerFields.slice();
		for ( var i = 0; i < playerFields.length; i++ )
		{
			const boardP = this.calcBoardPercent( playerFields[ i ] );
			if ( boardP > 0.5 )
			{
				console.log( "winnier is id: ", i );
        this.callback(i);
				return i;
			}
		}

		const mostFields = Math.max( ...playerFields );

    playerFields2.splice( playerFields2.indexOf( mostFields ), 1 )
		const secondMostFields = Math.max( ...playerFields2 );
		if ( secondMostFields + this.fieldsRemaining < mostFields )
		{
      const gameIdWinner = playerFields.indexOf( mostFields );
			console.log( gameIdWinner , "win" );
      this.callback(gameIdWinner);
			return gameIdWinner;
		}







		return false;



	}

	calcBoardPercent( numFields )
	{
		return numFields / ( this.sizeX * this.sizeY );
	}

	get fieldsRemaining()
	{
		let numFields = 0;

		for ( var i = 0; i < this.board.length; i++ )
		{
			for ( var j = 0; j < this.board[ i ].length; j++ )
			{
				if ( this.board[ i ][ j ].id == null )
				{
					numFields++;
				}
			}

		}
		return numFields;

	}




}

class Field
{
	constructor( x, y, color, game )
	{
		this.x = x;
		this.y = y;
		this.c = color;
		this.id = null;
		this.game = game;
	}

	setId( id )
	{
		this.id = id;
	}

	setColor( color )
	{
		this.c = color;
	}

	draw()
	{
		if ( this.id != null )
		{
			stroke( 1 );
			fill( this.c.r, this.c.g, this.c.b );
			rect( this.x * this.game.tileSize, this.y * this.game.tileSize, this.game.tileSize + this.x * this.game.tileSize, this.game.tileSize + this.y * this.game.tileSize );
		}
		else
		{
			noStroke();
			fill( this.c.r, this.c.g, this.c.b );
			rect( this.x * this.game.tileSize, this.y * this.game.tileSize, this.game.tileSize + this.x * this.game.tileSize, this.game.tileSize + this.y * this.game.tileSize );
		}
	}

	checkFieldsAround( colorChosen, playerId )
	{
		if ( this.id == playerId )
		{
			// console.log( "this is a check" );
			// console.log( this.x, this.y );

			var newColor = this.game.colors[ colorChosen - 1 ];
			this.game.board[ this.x ][ this.y ].c = newColor;

			// Init variables
			var fieldUp;
			var fieldDown;
			var fieldLeft;
			var fieldRight;

			var neighbors = [];

			// Get neighbors
			if ( this.y != 0 )
			{
				fieldUp = this.game.board[ this.x ][ this.y - 1 ];
				neighbors.push( fieldUp );
			}

			if ( this.y != this.game.sizeY - 1 )
			{
				fieldDown = this.game.board[ this.x ][ this.y + 1 ];
				neighbors.push( fieldDown );
			}

			if ( this.x != 0 )
			{
				fieldLeft = this.game.board[ this.x - 1 ][ this.y ];
				neighbors.push( fieldLeft );
			}

			if ( this.x != this.game.sizeX - 1 )
			{
				fieldRight = this.game.board[ this.x + 1 ][ this.y ];
				neighbors.push( fieldRight );
			}

			// console.log( neighbors );
			// console.log( "chosen color id: " + colorChosen );

			for ( var i = 0; i < neighbors.length; i++ )
			{
				if ( neighbors[ i ].id == null )
				{
					if ( neighbors[ i ].c.id == colorChosen )
					{
						neighbors[ i ].c = newColor;
						neighbors[ i ].id = playerId;
						neighbors[ i ].checkFieldsAround( colorChosen, playerId );
					}
				}
			}

		}
	}
}

class Player
{
  constructor(color, id, game)
  {
    this.color = color;
    this.id = id;
    this.game = game;
    this.nickname = `Player ${id}`;
  }

  setNickname(nickname){
    this.nickname = nickname;
  }

  chooseColor(colorId)
  {
    this.color = this.game.colors[colorId - 1];
    // console.log("player color: " , this.color);
    for (var i = 0; i < this.game.board.length; i++)
    {
      for (var j = 0; j < this.game.board[i].length; j++)
      {
        this.game.board[i][j].checkFieldsAround(colorId, this.id);
      }
    }

    this.game.checkWin();

  }
}

class RGB
{
	constructor( r, b, g, id )
	{
		this.r = r;
		this.b = b;
		this.g = g;
		this.id = id;
	}
}

if ( typeof module !== "undefined" && module.exports !== "undefined" )
{
	module.exports = Game;
}
