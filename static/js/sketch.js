var game = new Game();
const extraStuff = 250;
const colorWidth = 200;
const colorHeight = 50;
var board = [];
var myTurn = false;
var colorArray = [ game.colors[ 0 ], game.colors[ 1 ], game.colors[ 2 ], game.colors[ 3 ] ];
let canvas;

function setup()
{
	canvas = createCanvas( game.sizeX * game.tileSize + extraStuff, game.sizeY * game.tileSize + extraStuff );
	document.getElementById( "canvasContainer" )
		.appendChild( canvas.elt );



	background(52);



	//game.createBoard();
	//drawOnEvent();
}

function drawOnEvent()
{
	background( 52 );


	noStroke();
	for ( var x = 0; x < game.board.length; x++ )
	{
		for ( var y = 0; y < game.board[ x ].length; y++ )
		{
			game.board[ x ][ y ].draw();
		}
	}

	clearAround();

	drawPlayerPercent();

	if ( checkMyTurn( myGameId, currentTurn ) && gameStarted )
	{
		drawColors();
	}
	if (!gameStarted) {
		push();
		textAlign(CENTER, CENTER)
		text("Waiting for players...", width/2, height/2);
	}
}

function drawPlayerPercent()
{
	var playerAmount = game.players.length;
	var boardFields = game.getPlayersFields();
	//console.log( boardFields );
	var space = 75;

	var startX = 25;

	textSize( 30 );
	fill( 255 );
	for ( var i = 0; i < playerAmount; i++ )
	{
		let fields = boardFields[ i ];
		let gameBoardDecimal = game.calcBoardPercent( fields );
		let gameBoardPercent = gameBoardDecimal * 100;
		let roundedGameBoardDecimal = parseFloat( gameBoardPercent )
			.toFixed( 2 );
		let ystart = ( game.sizeY * game.tileSize ) + ( extraStuff / 2 );

		text( roundedGameBoardDecimal + "%", startX, ystart );
		text( nicknames[ i ], startX, ystart + 45 );
		startX += space * 2;
	}



}

function clearAround()
{

	// Starting width the side!
	fill( 52 );
	var startX = game.sizeX * game.tileSize;
	var startY = 0;
	var endX = game.sizeX * game.tileSize + extraStuff;
	var endY = game.sizeY * game.tileSize;

	rect( startX, startY, endX, endY );

	// Then taking the bottom and the corner

	var startX = 0;
	var startY = game.sizeY * game.tileSize;
	var endX = game.sizeX * game.tileSize + extraStuff;
	var endY = game.sizeY * game.tileSize + extraStuff;

	rect( startX, startY, endX, endY );
}

function drawColors()
{
	var colorAvailable = game.getAvailableColors();
	var sidegap = ( extraStuff - colorWidth ) / 2;
	var startX = game.sizeX * game.tileSize + sidegap;
	var endX = ( game.sizeX * game.tileSize + extraStuff ) - sidegap;
	var currentY = colorHeight;

	for ( var i = 0; i < colorAvailable.length; i++ )
	{
		fill( colorAvailable[ i ].r, colorAvailable[ i ].g, colorAvailable[ i ].b );
		rect( startX, currentY, colorWidth, colorHeight );
		currentY += colorHeight * 2;
	}
}

function mousePressed()
{
	var colorAvailable = game.getAvailableColors();
	var sidegap = ( extraStuff - colorWidth ) / 2;
	var startX = game.sizeX * game.tileSize + sidegap;
	var endX = ( game.sizeX * game.tileSize + extraStuff ) - sidegap;
	var currentY = colorHeight;

	for ( var i = 0; i < 4; i++ )
	{
		if ( mouseX > startX && mouseX < endX )
		{
			if ( mouseY > currentY && mouseY < ( currentY + colorHeight ) )
			{
				console.log( "click id: " + colorAvailable[ i ].id );
				room.send(
				{
					color: colorAvailable[ i ].id
				} );
			}
		}
		currentY += colorHeight * 2;
	}
}
