const host = window.document.location.host.replace(/:.*/, '');
const URLPort = ":" + location.port;
const port = 50000;
let client = new Colyseus.Client(location.protocol.replace("http", "ws")  + host + (port ? ':'+port : ''));
var room;
var game;
var myGameId;
var currentTurn =0;
var gameStarted = false;
var nicknames = ["Player 0", "Player 1", "Player 2", "Player 3"];


var mainMenuDiv;
// const namePageDiv = document.getElementById("chooseNamePage");
var gamePageDiv ;
var pages;

const hideAllPages = () => {
  // hide other poges

  pages.forEach(page => page.style.display = "none");
}

const showMainMenu = () => {
  // hide other poges
  hideAllPages();
  //show page
  mainMenuDiv.style.display = "block";

}

const showNamePage = () => {
  // hide other poges
  namePageDiv.style.display = "block";
}

const showGame = () => {
  // hide other poges
  hideAllPages();
  gamePageDiv.style.display = "block";
}



const clientReadyToStart = () => {
  if (typeof myGameId !== "undefined" && game.board.length > 0 && gameStarted) {
    return true;
  } else {
    return false;
  }
}


const joinRoom = (roomId, options) => {
  //join the room
  room = client.join(roomId, options);


  room.onStateChange.addOnce(function(state) {
    //console.log("this is the first room state!", state);
    //game.setBoard(state.map);
    if (typeof state.players[client.id] == "object") {
      myGameId = state.players[client.id].gameId;
    }

  });

  room.onStateChange.add(function(state) {
    //console.log("the room state has been updated:", state);
    //game.setBoard(state.map);
    if (typeof state.players[client.id] == "object") {
      myGameId = state.players[client.id].gameId;
    }

    drawOnEvent();
  });



  room.onMessage.add(function(message) {
    //console.log("server just sent this message:");

    if (message.action == "choose color") {
      //color
      //player game id
      game.players[message.gameId].chooseColor(message.colorId);



    } else if (message.action == "changed nickname") {
      nicknames[message.gameId] = message.nickname;

    } else if (message.action == "nicknames") {
      message.nicknames.forEach(nicknameObject => {
        nicknames[nicknameObject.gameId] = nicknameObject.nickname;
      });
    } else if (message.action == "chat") {
      addMessage(message);
    }

    if (message.gameId) {
      myGameId = message.gameId;
      if (clientReadyToStart()) {
        //start spillet
      }
    } else if (message.message == "start game") {
      //set map
      //set gameId
      game.setBoard(message.map);
      myGameId = message.players[client.id].gameId;
      gameStarted = true;
      for (let playerId in message.players) {
        let cPlayer = message.players[playerId]
        game.addPlayer(game.colors[cPlayer.gameId], cPlayer.gameId);

      }

      drawOnEvent();
    }

    // console.log(message);
  });

  room.onJoin.add(function() {
     console.log(`client joined successfully room: ${room.id}`);
     showGame();
     //set room link input
     $("#roomLinkBox").val(`${location.protocol}//${host}${URLPort}/room/${room.id}`);


     room.send("gameId");


    // console.log(room);
    //instantiate game

    game = new Game(winnerId => {
      const winnerString = `${nicknames[winnerId]} won the game`;
      console.log(winnerString);
      alert(winnerString);
    });


  });

  room.onLeave.add(function() {
    console.log("client left the room");
  });

  room.onError.add(function(err) {
    console.log("oops, error ocurred:");
    console.log(err);
  });

  room.listen("turn", change =>{
    //check if it is this clients turn
    console.log("change: " , change);
    currentTurn = change.value;
    drawOnEvent();

  });

  room.listen("gameStarted", change => {

    gameStarted = change.value;
    startGameIfReady();

  });

}


const addMessage = messageObject => {
  const li = document.createElement("li");
  li.innerHTML = messageObject.message;
  document.querySelector("#messages").appendChild(li);
}


const createColorify = options => {
  joinRoom("colorify", options);
  $("#createColorifyButton").prop("disabled", true);
  //display creating room or change screen imidietly
}





const createOrJoinGame = () => {
  const nickname = document.getElementById("nicknameInput");
  if (nickname == null || typeof nickname == "undefined" || nickname === "") {
    alert("you have to choose a nickname");
    return;
  }

  let checkRoom = location.href.match(/room\/([a-zA-Z0-9\-_]+)/);
  if (checkRoom == null) {
    createColorify({
      nickname: nickname
    })
  } else {
    joinRoom(checkRoom, {
      id: roomId,
      nickname: nickname
    });
  }
}


const startGameIfReady = () => {
  // check client ready
  if (clientReadyToStart()) {
    // if true draw map then

    drawOnEvent();

  }

}


const changeNickname = () => {
  const nickname = $("#nicknameInput").val()
  console.log("changing nickname", nickname);
  room.send({
    nickname: nickname
  });
}


$(document).ready(() => {
  mainMenuDiv = document.getElementById("mainMenu");
  gamePageDiv = document.getElementById("gamePage");
  pages = [mainMenuDiv, gamePageDiv];


  let checkRoom = location.href.match(/room\/([a-zA-Z0-9\-_]+)/);
  if (checkRoom == null) {
    //load normal page
    console.log("Main menu");
    showMainMenu();



  } else {
    console.log("game page");


    let roomId = checkRoom[1];
    console.log(`joining room: ${roomId}`);

    joinRoom(roomId, {id: roomId});
  }

  const roomLinkBox = $("#roomLinkBox");
  const copyButton = $("#roomLinkCopyButton")
  // console.log(roomLinkBox, "setting listener");
  copyButton.click(() => {
    // console.log("select");
    roomLinkBox[0].select();
    document.execCommand("copy");
  });

});
