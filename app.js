const colyseus = require("colyseus");
const http = require("http");
const express = require("express");
const WebSocket = require("ws");
const colorify = require('./rooms/colorify.js');
const path = require('path');


const app = express();
const gameServer = new colyseus.Server({
  engine: WebSocket.Server,
  server: http.createServer(app)

  //,presence: new colyseus.RedisPresence()
});

// room registration
gameServer.register("colorify", colorify);


//the content server
app.use('/', express.static(path.join(__dirname, "static")));

app.get('/room/:id', (req, res) => {
  res.sendFile(path.join(__dirname, "static/index.html"));
});


const port = process.env.PORT || 50000;
gameServer.listen(port);

console.log("app running on port " + port);
